//
//  MyClass.swift
//  Extension
//
//  Created by Aleksey Knysh on 2/26/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import Foundation

protocol ForExtension {
}

protocol IdenticalProtocolForExtension {
}

// не возможно подписать протоколы на расширение так как расширение является приватным

// MARK: - class MyClass
class MyClass {
    var name = "Vasia"
    
    func functionСall() {
        print("Вертолёт находится в точке высадки")
        firstFunc()
    }
}

private extension MyClass {
    func firstFunc() {
        print("Первый пошёл ...")
        secondFunc()
    }
    
    func secondFunc() {
        print("Второй пошёл ...")
        thirdFunc()
    }
    
    func thirdFunc() {
        print("Третий на подходе ...")
        firstFunc()
    }
    
    // не возможно переопредилить метод класса в протоколе, так как расширения только добовляют функцианал, но не могут переопределять или менять уже существующий.
}
