//
//  ViewController.swift
//  Extension
//
//  Created by Aleksey Knysh on 2/25/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let myClass = MyClass()
        
        let myColor: UIColor = .random
        print(myColor)
        
        let arrayInt = [1, 2, 3, 1, 2, 4, 5, 6, 7, 7]
        print(arrayInt.arrayWithoutRepetitions(arrayElement: arrayInt))
        
        print(myClass.name)
        
        myClass.functionСall()
        
    }
}

extension UIColor {
    static var random: UIColor {
        UIColor(red: .random(in: 0...1),
                green: .random(in: 0...1),
                blue: .random(in: 0...1),
                alpha: 1.0)
    }
}

extension Array where Element: Hashable {
    func arrayWithoutRepetitions(arrayElement: [Element]) -> [Element] {
        let set = Set<Element>(arrayElement)
        let array = Array(set)
        return array
    }
}
